ceilometer (1:19.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 06 Oct 2022 08:56:19 +0200

ceilometer (1:19.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Sep 2022 15:02:11 +0200

ceilometer (1:19.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed python3-pysnmp4 (build-)depends.
  * Removed pysnmp4-cmdgen-has-moved.patch now irrelevant.
  * Do not install loadbalancer_v2_meter_definitions.yaml (doesn't exist
    upstream anymore).

 -- Thomas Goirand <zigo@debian.org>  Tue, 13 Sep 2022 11:11:18 +0200

ceilometer (1:18.0.0-2) unstable; urgency=medium

  * Fix shell for nova when creating the user (ie: dash instead of false).

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 Jun 2022 12:04:43 +0200

ceilometer (1:18.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Mar 2022 15:54:28 +0200

ceilometer (1:18.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Mar 2022 15:03:06 +0100

ceilometer (1:18.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Mar 2022 21:44:07 +0100

ceilometer (1:17.0.0-2) unstable; urgency=medium

  * Removed (build-)dependency on ceilometerclient.

 -- Thomas Goirand <zigo@debian.org>  Fri, 11 Feb 2022 12:08:01 +0100

ceilometer (1:17.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 06 Oct 2021 17:09:57 +0200

ceilometer (1:17.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.
  * Add missing oslo.reports namespace when generating config file.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Sep 2021 22:15:20 +0200

ceilometer (1:17.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 15 Sep 2021 13:52:06 +0200

ceilometer (1:16.0.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 12:03:23 +0200

ceilometer (1:16.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 14 Apr 2021 23:33:53 +0200

ceilometer (1:16.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed (build-)depends versions when satisfied in Bullseye.
  * debhelper-compat 11.
  * Standards-Version: 4.5.1.
  * d/rules: removed --with systemd.
  * Fixed (build-)depends for this release.
  * Refreshed pysnmp4-cmdgen-has-moved.patch.

 -- Thomas Goirand <zigo@debian.org>  Tue, 23 Mar 2021 12:52:51 +0100

ceilometer (1:15.0.0-3) unstable; urgency=medium

  * Do not symlink gnocchi_resources.yaml, just copy it in /etc/ceilometer, as
    it has precedence in the Ceilometer code anyway.
  * Remove Breaks: + Replaces: python3-ceilometer (<< 1:15.0.0-2~), in fact not
    needed (we're not moving a file).

 -- Thomas Goirand <zigo@debian.org>  Wed, 03 Mar 2021 14:25:36 +0100

ceilometer (1:15.0.0-2) unstable; urgency=medium

  * Move gnocchi_resources.yaml in ceilometer-common:/etc/ceilometer, so that
    it becomes a CONFFILE.

 -- Thomas Goirand <zigo@debian.org>  Wed, 03 Mar 2021 10:49:38 +0100

ceilometer (1:15.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Sat, 17 Oct 2020 16:22:15 +0200

ceilometer (1:15.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 24 Sep 2020 15:11:49 +0200

ceilometer (1:14.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 May 2020 15:14:49 +0200

ceilometer (1:14.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 09 May 2020 23:31:57 +0200

ceilometer (1:14.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Apr 2020 19:28:07 +0200

ceilometer (1:13.1.0-1) unstable; urgency=medium

  * New upstream point release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 26 Mar 2020 12:53:01 +0100

ceilometer (1:13.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Run wrap-and-sort -bastk.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Oct 2019 23:50:54 +0200

ceilometer (1:13.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 16 Oct 2019 17:46:49 +0200

ceilometer (1:13.0.0~rc1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast.
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Also install ceilometer-status.

 -- Thomas Goirand <zigo@debian.org>  Thu, 26 Sep 2019 21:24:35 +0200

ceilometer (1:12.0.0-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Michal Arbet <michal.arbet@ultimum.io>  Wed, 17 Jul 2019 15:21:05 +0200

ceilometer (1:12.0.0-2) experimental; urgency=medium

  * d/control:
      - Bump openstack-pkg-tools to version 99
      - Add me to uploaders field
  * d/copyright: Add me to copyright file

 -- Michal Arbet <michal.arbet@ultimum.io>  Fri, 03 May 2019 18:39:20 +0200

ceilometer (1:12.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 11 Apr 2019 08:55:25 +0200

ceilometer (1:12.0.0~rc1-2) experimental; urgency=medium

  * Do not use python3- prefix when calling oslo-config-generator.

 -- Thomas Goirand <zigo@debian.org>  Fri, 29 Mar 2019 16:45:24 +0100

ceilometer (1:12.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed package versions when satisfied in Buster.
  * Add missing (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Fri, 29 Mar 2019 15:27:42 +0100

ceilometer (1:11.0.1-4) unstable; urgency=medium

  * Add missing /etc/ceilometer/polling.yaml file.

 -- Thomas Goirand <zigo@debian.org>  Thu, 31 Jan 2019 15:37:53 +0100

ceilometer (1:11.0.1-3) unstable; urgency=medium

  * Removed mongodb build-depends (Closes: #919088).

 -- Thomas Goirand <zigo@debian.org>  Wed, 23 Jan 2019 11:33:54 +0100

ceilometer (1:11.0.1-2) unstable; urgency=medium

  * Drop ceilometer-alarm-{evaluator,notifier} transition packages.

 -- Thomas Goirand <zigo@debian.org>  Thu, 17 Jan 2019 16:29:26 +0100

ceilometer (1:11.0.1-1) unstable; urgency=medium

  * New upstream point release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 13 Dec 2018 17:38:01 +0100

ceilometer (1:11.0.0-3) unstable; urgency=medium

  * Fix pysnmp's cmdgen module path, fixing FTBFS (Closes: #909985).

 -- Thomas Goirand <zigo@debian.org>  Mon, 01 Oct 2018 09:38:27 +0200

ceilometer (1:11.0.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Don't use wait_for_line, just wait 10 seconds.

 -- Thomas Goirand <zigo@debian.org>  Fri, 24 Aug 2018 23:27:17 +0200

ceilometer (1:11.0.0-1) experimental; urgency=medium

  [ Thomas Goirand ]
  * Fixed better init description.
  * New upstream release.
  * Fixed (build-)depends for this release.

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

 -- Thomas Goirand <zigo@debian.org>  Fri, 15 Jun 2018 09:51:03 +0000

ceilometer (1:10.0.0-5) unstable; urgency=medium

  * Add missing ceilometer-polling service.

 -- Thomas Goirand <zigo@debian.org>  Mon, 07 May 2018 09:37:25 +0200

ceilometer (1:10.0.0-4) unstable; urgency=medium

  * Using debconf templates from openstack-pkg-tools, dropping our own.

 -- Thomas Goirand <zigo@debian.org>  Mon, 05 Mar 2018 22:57:29 +0000

ceilometer (1:10.0.0-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 27 Feb 2018 15:12:05 +0000

ceilometer (1:10.0.0-2) experimental; urgency=medium

  * Some more Python 3 switch fixes.

 -- Thomas Goirand <zigo@debian.org>  Thu, 22 Feb 2018 15:28:35 +0000

ceilometer (1:10.0.0-1) experimental; urgency=medium

  [ Thomas Goirand ]
  * Fixed default lock_path in config file.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * Running wrap-and-sort -bast

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed ceilometer-{collector,api} (it's gone upstream too). Make the
    ceilometer-common package Conflicts: on them to ensure removal.
  * Standards-Version: is now 4.1.3.
  * Fixed ceilometer-agent-central short desc.
  * Switch package to Python 3.

 -- Thomas Goirand <zigo@debian.org>  Sat, 17 Feb 2018 10:46:25 +0000

ceilometer (1:9.0.4-1) unstable; urgency=medium

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed ceilometer-api UWSGI startup.

  [ David Rabel ]
  * Update fr.po (Closes: #885588).

 -- Thomas Goirand <zigo@debian.org>  Fri, 26 Jan 2018 08:56:12 +0100

ceilometer (1:9.0.1-2) unstable; urgency=medium

  * Uploading to unstable.
  * (build-)depends on python-zaqarclient (Closes: #880129).
  * Updated pt.po (Closes: #872179).
  * Removed dh-systemd build-depends.

 -- Thomas Goirand <zigo@debian.org>  Sun, 29 Oct 2017 20:22:18 +0100

ceilometer (1:9.0.1-1) experimental; urgency=medium

  * New upstream release.
  * Ran wrap-and-sort -bast.
  * Updated VCS URLs.
  * Updating maintainer field.
  * Updating standards version to 4.1.1.
  * Fixed (build-)depends for this release.
  * Minor debian/rules clean-ups.
  * Fixed namespaces for ceilometer.conf.
  * Fixed ceilometer-common.install (updated to match upstream).

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Oct 2017 02:37:14 +0200

ceilometer (1:7.0.1-5) unstable; urgency=medium

  * Fix ceilometer-api: add "--" as DAEMON_ARGS (Closes: #861199).
  * Also fix the default port to 8777 as it should (instead of the default
    port 8000 which is wrong).

 -- Thomas Goirand <zigo@debian.org>  Fri, 28 Apr 2017 10:51:50 +0200

ceilometer (1:7.0.1-4) unstable; urgency=medium

  * Fix ceilometer-agent-central .service file (Closes: #861202).
  * Fix ceilometer-agent-compute .service file (Closes: #861203).

 -- Thomas Goirand <zigo@debian.org>  Wed, 26 Apr 2017 15:36:56 +0000

ceilometer (1:7.0.1-3) unstable; urgency=medium

  * Re-integrate NMU from Piotr fixing SQLAlchemy << 1.1 dependency.
  * German debconf translation update (Closes: #842484).

 -- Thomas Goirand <zigo@debian.org>  Mon, 03 Apr 2017 17:53:05 +0200

ceilometer (1:7.0.1-2) unstable; urgency=medium

  * Team upload.
  * Use --gecos instead of -gecos with adduser (Closes: #846247).
  * Bump build dependency on openstack-pkg-tools (Closes: #858684).

 -- David Rabel <david.rabel@noresoft.com>  Sat, 01 Apr 2017 11:20:23 +0200

ceilometer (1:7.0.1-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Patch requirements.txt to drop SQLAlchemy << 1.1 dependency

 -- Piotr Ożarowski <piotr@debian.org>  Fri, 03 Feb 2017 23:21:31 +0100

ceilometer (1:7.0.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * New upstream release
  * d/s/options: extend-diff-ignore of .gitreview
  * d/control: Use correct branch in Vcs-* fields
  * Bumped debhelper compat version to 10

  [ Thomas Goirand ]
  * Install missing files (Closes: #848586).

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Dec 2016 16:57:59 +0100

ceilometer (1:7.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 06 Oct 2016 17:30:44 +0200

ceilometer (1:7.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.
  * Build-Depends on openstack-pkg-tools (>= 53~).
  * Fixed EPOCH for oslotest.
  * Debconf translation:
    - it (Closes: #838500).

 -- Thomas Goirand <zigo@debian.org>  Wed, 28 Sep 2016 09:37:20 +0200

ceilometer (1:7.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Using OpenStack's Gerrit as VCS URLs.
  * Points .gitreview to OpenStack packaging-deb's Gerrit.
  * Fixed installation of files in /etc/ceilometer for this release.
  * Also install /usr/bin/ceilometer-upgrade.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Sep 2016 22:58:16 +0200

ceilometer (1:7.0.0~b2-1) experimental; urgency=medium

  [ Thomas Goirand ]
  * Updated Danish translation of the debconf templates (Closes: #830640).
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Add debian/function.sh as it's gone away upstream.
  * Black list failed unit test:
    - agent.test_manager.TestRunTasks.test_batching_polled_samples_false
  * Also package /usr/bin/ceilometer-db-legacy-clean.

 -- Thomas Goirand <zigo@debian.org>  Mon, 18 Jul 2016 19:57:15 +0200

ceilometer (1:6.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)
  * d/copyright: Changed source URL to new one

  [ Thomas Goirand ]
  * Updated Japanese debconf templates (Closes: #820760).
  * Updated Dutch debconf templates (Closes: #822966).
  * Added Brazilian Portuguese debconf templates (Closes: #824295).
  * Check if /usr/bin/ceilometer-collector is there before attempting to call
    /usr/bin/ceilometer-expirer in the daily cron, so that there's no risk to
    have an output if the ceilometer-collector package is removed
    (Closes: #822897).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 May 2016 10:48:09 +0200

ceilometer (1:6.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 07 Apr 2016 21:30:44 +0200

ceilometer (1:6.0.0~rc3-1) unstable; urgency=medium

  [ Ivan Udovichenko ]
  * d/ceilometer-agent-*.init.in: Fix DAEMON_ARGS for
    central, compute and ipmi agents.

  [ Thomas Goirand ]
  * New upstream release.
  * Uploading to unstable.
  * Updated ja.po debconf translation (Closes: #816342).

 -- Thomas Goirand <zigo@debian.org>  Tue, 05 Apr 2016 10:27:08 +0200

ceilometer (1:6.0.0~rc1-2) experimental; urgency=medium

  * Do not use keystone admin auth token to register the API endpoint.

 -- Thomas Goirand <zigo@debian.org>  Tue, 29 Mar 2016 12:22:36 +0000

ceilometer (1:6.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Added git to build-depends-indep.
  * Fixed (build-)depends for this release.
  * Fixed unit test runner (upstream script ./setup-test-env-mongodb.sh isn't
    available anymore: using one in the debian folder).
  * Standards-Version: 3.9.7 (no change).

 -- Thomas Goirand <zigo@debian.org>  Thu, 10 Mar 2016 13:17:08 +0100

ceilometer (1:6.0.0~b3-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Disable test:
    meter.test_notifications.TestMeterProcessing.test_fallback_meter_path

 -- Thomas Goirand <zigo@debian.org>  Fri, 04 Mar 2016 00:05:14 +0800

ceilometer (1:6.0.0~b2-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * The alarm packages are now transition packages to Aodh.
  * Fixed debian/copyright ordering.
  * Updated namespaces when generating the config file.

 -- Thomas Goirand <zigo@debian.org>  Tue, 08 Dec 2015 11:53:07 +0100

ceilometer (1:5.0.0-5) unstable; urgency=medium

  * Added q-text-as-data as depends for ceilometer-api.

 -- Thomas Goirand <zigo@debian.org>  Tue, 03 Nov 2015 11:28:44 +0000

ceilometer (1:5.0.0-4) unstable; urgency=medium

  * Rebuilt with openstack-pkg-tools to use Keystone API v3.

 -- Thomas Goirand <zigo@debian.org>  Tue, 03 Nov 2015 09:30:52 +0000

ceilometer (1:5.0.0-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Oct 2015 13:37:36 +0000

ceilometer (1:5.0.0-2) experimental; urgency=medium

  * Fixed ceilometer-common.postinst.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Oct 2015 19:41:24 +0200

ceilometer (1:5.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Now checking for libvirt or libvirtd group existance before guessing it
    with "dpkg-vendor --derives-from ubuntu".

 -- Thomas Goirand <zigo@debian.org>  Mon, 12 Oct 2015 10:50:26 +0200

ceilometer (1:5.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Get the ceilometer-agent-central use "ceilometer-polling --central".
  * Get the ceilometer-agent-compute to use "ceilometer-polling --namespace
    compute".
  * Updated nl.po
  * Now ceilometer-api depends on python-openstackclient.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 12 Sep 2015 15:26:48 +0000

ceilometer (1:5.0.0~b3-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Realign Ceilometer packaging with MOS.
  * Not using transition packages for sphinxcontrib stuff.
  * Do not run functiona tests when building.

 -- Thomas Goirand <zigo@debian.org>  Mon, 31 Aug 2015 20:55:14 +0200

ceilometer (1:5.0.0~b2-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Not using sphinxcontrib transition packages anymore.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Aug 2015 13:05:25 +0200

ceilometer (2015.1.0-6) unstable; urgency=medium

  * Updated French debconf translations thanks to Julien Patriarca
    (Closes: #789902).

 -- Thomas Goirand <zigo@debian.org>  Fri, 26 Jun 2015 15:11:26 +0200

ceilometer (2015.1.0-5) unstable; urgency=medium

  * Added debconf screen to ask if dbsync should be done (Closes: #787536).
  * Put ceilometer-doc in the doc section.

 -- Thomas Goirand <zigo@debian.org>  Thu, 11 Jun 2015 09:18:13 +0200

ceilometer (2015.1.0-4) unstable; urgency=medium

  * Added a documentation package with the sphinx docs.

 -- Thomas Goirand <zigo@debian.org>  Thu, 04 Jun 2015 09:40:08 +0000

ceilometer (2015.1.0-3) unstable; urgency=medium

  * Added ceilometer-polling as new package.
  * Removed Pre-Depends on dpkg.

 -- Thomas Goirand <zigo@debian.org>  Thu, 04 Jun 2015 11:10:31 +0200

ceilometer (2015.1.0-2) unstable; urgency=medium

  * Added missing --namespace oslo.policy when generating default conf.

 -- Thomas Goirand <zigo@debian.org>  Fri, 15 May 2015 16:22:51 +0200

ceilometer (2015.1.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Apr 2015 21:17:17 +0000

ceilometer (2015.1~rc2-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Reviewed (build-)depends for this release.
  * Fixed default connection sed command line.

 -- Thomas Goirand <zigo@debian.org>  Tue, 23 Dec 2014 14:47:10 +0800

ceilometer (2014.2.1-1) experimental; urgency=medium

  * New upstream release.
  * Using auth_protocol=http by default.

 -- Thomas Goirand <zigo@debian.org>  Sun, 14 Dec 2014 16:04:27 +0800

ceilometer (2014.2-3) experimental; urgency=medium

  * Added argparse & ordereddict to debian/pydist-overrides.

 -- Thomas Goirand <zigo@debian.org>  Sun, 19 Oct 2014 10:58:28 +0000

ceilometer (2014.2-2) experimental; urgency=medium

  * Added missing python-posix-ipc as (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Sun, 19 Oct 2014 15:13:52 +0800

ceilometer (2014.2-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 16 Oct 2014 14:47:57 +0000

ceilometer (2014.2~rc3-1) experimental; urgency=medium

  * New upstream release.
  * Added missing configuration files in ceilometer-common.
  * Removed Add_oslo.db_to_config_generator.patch applied upstream.

 -- Thomas Goirand <zigo@debian.org>  Wed, 15 Oct 2014 14:02:53 +0800

ceilometer (2014.2~rc1-4) experimental; urgency=medium

  * Adds patch for generating the config file correctly.

 -- Thomas Goirand <zigo@debian.org>  Fri, 10 Oct 2014 14:32:36 +0000

ceilometer (2014.2~rc1-3) experimental; urgency=medium

  * Using a single unique ceilometer-common logrotate file.

 -- Thomas Goirand <zigo@debian.org>  Tue, 07 Oct 2014 13:45:40 +0800

ceilometer (2014.2~rc1-2) experimental; urgency=medium

  * Mangling upstream rc and beta versions in watch file.
  * Added missing binaries.
  * Now packaging ceilometer-agent-ipmi.
  * Using templated init, upstart and systemd scripts from
    openstack-pkg-tools >= 13.
  * Fixed upstream files gone in debian/copyright.
  * Standards-Version is now 3.9.6 (no change).

 -- Thomas Goirand <zigo@debian.org>  Tue, 07 Oct 2014 00:49:22 +0000

ceilometer (2014.2~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Updated (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 24 Sep 2014 09:39:11 +0800

ceilometer (2014.2~b3-1) experimental; urgency=medium

  * New upstream release.
  * Generating config file from script, since it's gone from upstream.
  * Removed all patches, all applied upstream.
  * Removed sources.json, as it's gone away upstream.

 -- Thomas Goirand <zigo@debian.org>  Tue, 01 Jul 2014 15:01:05 +0800

ceilometer (2014.1.1-2) unstable; urgency=medium

  * Updated de.po thanks to Chris Leick <c.leick@vollbio.de> (Closes: #751164).

 -- Thomas Goirand <zigo@debian.org>  Wed, 11 Jun 2014 12:29:07 +0800

ceilometer (2014.1.1-1) unstable; urgency=medium

  * New upstream release.
  * Bumped python-six minimal version to 1.6.0.

 -- Thomas Goirand <zigo@debian.org>  Mon, 09 Jun 2014 21:46:22 +0800

ceilometer (2014.1-7) unstable; urgency=medium

  * Switched from restarting daemons to copytruncate for logrotate.

 -- Thomas Goirand <zigo@debian.org>  Thu, 29 May 2014 13:51:53 +0800

ceilometer (2014.1-6) unstable; urgency=medium

  * Adds Add_aggregator_transformer.patch.

 -- Thomas Goirand <zigo@debian.org>  Thu, 22 May 2014 00:06:15 +0800

ceilometer (2014.1-5) unstable; urgency=medium

  * Added Opencontrail_network_statistics_driver.patch.
  * Added version depends for python-pysnmp4 (now >= 4.2.1).

 -- Thomas Goirand <zigo@debian.org>  Wed, 21 May 2014 08:20:48 +0800

ceilometer (2014.1-4) unstable; urgency=medium

  * ceilometer now depends on version >= 2:2.17.0 of novaclient.

 -- Thomas Goirand <zigo@debian.org>  Fri, 09 May 2014 22:59:36 +0800

ceilometer (2014.1-3) unstable; urgency=medium

  * Sets /etc/ceilometer/pipeline.yaml as conffile and remove handling from the
    from maintainer scripts (Closes: #747216).
  * Did the same for etc/ceilometer/{policy,sources}.json.

 -- Thomas Goirand <zigo@debian.org>  Tue, 06 May 2014 23:00:59 +0800

ceilometer (2014.1-2) unstable; urgency=medium

  * Fixed long description typo (Closes: #745321).
  * Delete var/lib/ceilometer & /var/log/ceilometer on purge (Closes: #732457).
  * Updated it.po debconf translation (Closes: #745387).

 -- Thomas Goirand <zigo@debian.org>  Fri, 02 May 2014 19:20:36 +0800

ceilometer (2014.1-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Documents: using mongodb by default.
  * Added selection of logging (to file or syslog) thanks to Sylvain Baubeau
    <sylvain.baubeau@enovance.com>.

 -- Thomas Goirand <zigo@debian.org>  Fri, 18 Apr 2014 00:23:00 +0800

ceilometer (2014.1~rc1-1) experimental; urgency=low

  * New upstream release.
  * Better testr/subunit output.

 -- Thomas Goirand <zigo@debian.org>  Sun, 30 Mar 2014 13:25:59 +0800

ceilometer (2014.1~b3-1) experimental; urgency=low

  * New upstream release (Icehouse beta 3).
  * Removes now applied upstream CVE-2013-6384 patch, refreshes
    removes-sqlalchemy-restriction.patch
  * Added msgpack_python python-msgpack in pydist-overrides, as otherwise the
    python-msgpack-python is automatically added as dependency.

 -- Thomas Goirand <zigo@debian.org>  Sat, 22 Mar 2014 14:19:25 +0800

ceilometer (2013.2.2-2) unstable; urgency=medium

  * Rebuilt Ceilometer with the new openstack-pkg-tools >= 9.

 -- Thomas Goirand <zigo@debian.org>  Fri, 14 Feb 2014 17:24:30 +0000

ceilometer (2013.2.2-1) unstable; urgency=medium

  * New upstream point release.
  * refreshed patches.

 -- Thomas Goirand <zigo@debian.org>  Fri, 14 Feb 2014 10:19:27 +0800

ceilometer (2013.2.1-5) unstable; urgency=medium

  * Added missing postrotate scripts to restart daemons after logrotate
    (Closes: #736930).

 -- Thomas Goirand <zigo@debian.org>  Mon, 03 Feb 2014 15:41:29 +0800

ceilometer (2013.2.1-4) unstable; urgency=medium

  * Fix ${LIBVIRT_GROUP} when doing adduser ceilometer libvirt, so that it
  * also
    works on Ubuntu.

 -- Thomas Goirand <zigo@debian.org>  Wed, 15 Jan 2014 17:37:07 +0800

ceilometer (2013.2.1-3) unstable; urgency=medium

  * Adds ceilometer/api/app.wsgi to /usr/share/ceilometer.
  * Added/updated debconf templates thanks to:
    - German: Martin E. Schauer (Closes: #734739)
    - Swedish: Martin Bagge (Closes: #734583)
    - French: Julien Patriarca (Closes: #733092)
    - Spanish: Matias A. Bellone (Closes: #732534)

 -- Thomas Goirand <zigo@debian.org>  Mon, 09 Dec 2013 17:09:32 +0800

ceilometer (2013.2-5) unstable; urgency=medium

  * Adds call to ceilometer-expirer every day.

 -- Thomas Goirand <zigo@debian.org>  Thu, 05 Dec 2013 00:30:09 +0800

ceilometer (2013.2-4) unstable; urgency=low

  * CVE-2013-6384: applied upstream patch mongodb, db2: do not print full
    URL in logs (Closes: #730227).
  * Switches from msgpack-python to python-msgpack in dependencies, as the
    package has been renamed (Closes: #730874).
  * Updates some debconf translations, with warm thanks to:
    - French, Julien Patriarca <leatherface@debian.org> (Closes: #728771).
    - Russian, Yuri Kozlov <yuray@komyakino.ru> (Closes: #729774).

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Dec 2013 19:41:05 +0800

ceilometer (2013.2-3) unstable; urgency=medium

  * Added missing (build-)depends: python-six (>= 1.4.1).

 -- Thomas Goirand <zigo@debian.org>  Tue, 26 Nov 2013 22:37:47 +0800

ceilometer (2013.2-2) unstable; urgency=low

  * Added configuration of the keystone_authtoken through Debconf.
  * Fixed ${LIBVIRT_GROUP} instead of just libvirt in postinst to have it work
    as well on Ubuntu.

 -- Thomas Goirand <zigo@debian.org>  Mon, 28 Oct 2013 22:34:50 +0800

ceilometer (2013.2-1) unstable; urgency=low

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 17 Oct 2013 23:37:48 +0800

ceilometer (2013.2~rc2-3) experimental; urgency=low

  * Ceilometer needs python-wsme at least 0.5b6, fixed (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Thu, 17 Oct 2013 15:04:01 +0800

ceilometer (2013.2~rc2-2) experimental; urgency=low

  * Added the 2 new services: alarm-evaluator and alarm-notifier.

 -- Thomas Goirand <zigo@debian.org>  Mon, 14 Oct 2013 23:52:15 +0800

ceilometer (2013.2~rc2-1) experimental; urgency=low

  * New upstream pre-release.
  * Increased python-keystoneclient (build-)depends to 0.4.0.

 -- Thomas Goirand <zigo@debian.org>  Mon, 14 Oct 2013 16:47:55 +0800

ceilometer (2013.2~rc1-1) experimental; urgency=low

  * New upstream pre-release 2013.2.rc1.

 -- Thomas Goirand <zigo@debian.org>  Sat, 29 Jun 2013 01:45:17 +0800

ceilometer (2013.1.3-2) unstable; urgency=low

  * Added new Debconf translations:
    - FR (Closes: #722418).
    - DA (Closes: #721548).
    - RU (Closes: #721302).

 -- Thomas Goirand <zigo@debian.org>  Wed, 25 Sep 2013 16:19:22 +0800

ceilometer (2013.1.3-1) unstable; urgency=low

  * New upstream point release.
  * Added several Debconf translations:
    - Italian, thanks to Beatrice Torracca (Closes: #719709).
    - Japanese, thanks to victory (Closes: #719722).
    - Portuguese, thanks to the Traduz team (Closes: #720382).
    - Czech, thanks to Michal Šimůnek (Closes: #721218).

 -- Thomas Goirand <zigo@debian.org>  Fri, 30 Aug 2013 11:19:44 +0800

ceilometer (2013.1.2-4) unstable; urgency=low

  * Ran debconf-updatepo.

 -- Thomas Goirand <zigo@debian.org>  Thu, 11 Jul 2013 01:07:43 +0800

ceilometer (2013.1.2-3) unstable; urgency=low

  * Added debian-l10n-english review (Closes #708747).
  * Do not fail on directory removal on purge (Closes: #710910).

 -- Thomas Goirand <zigo@debian.org>  Wed, 10 Jul 2013 14:17:03 +0800

ceilometer (2013.1.2-2) unstable; urgency=low

  * Added some Should-Start / Should-Stop so that Ceilometer starts after Mongo
    and RabbitMQ.
  * Removed version-depends for sqlalchemy.

 -- Thomas Goirand <zigo@debian.org>  Sat, 22 Jun 2013 15:50:03 +0800

ceilometer (2013.1.2-1) unstable; urgency=low

  * New upstream release.
  * Added configuration to each init scripts so that they log in their own log
    file, added the corresponding purge postrm and logrotate.
  * Removed applied upstream patch: modify-limitation-on-request-version.patch
  * Build-depends on python-happybase and python-swift.
  * Now runs the unit tests at build time.

 -- Thomas Goirand <zigo@debian.org>  Mon, 03 Jun 2013 16:26:14 +0800

ceilometer (2013.1.1-1) unstable; urgency=low

  * New upstream release.
  * rmdir --ignore-fail-on-non-empty of few folders owned by nova after purge.
    Thanks to Andreas Beckmann for reporting (Closes: #709876).
  * Ran wrap-and-sort to clean debian/control.
  * Replaced pkgos_var_user_group nova by pkgos_adduser, as we don't want to
    create the log / lib dirs of nova (Closes: #709876).
  * Creates the correct libvirt group if the package is installed in Ubuntu.
  * Bumped Standard-Version to 3.9.4.

 -- Thomas Goirand <zigo@debian.org>  Mon, 27 May 2013 11:07:59 +0800

ceilometer (2013.1-3) unstable; urgency=low

  * Adds modify-limitation-on-request-version.patch without wich Ceilometer
    doesn't work at all.

 -- Thomas Goirand <zigo@debian.org>  Tue, 21 May 2013 17:27:07 +0800

ceilometer (2013.1-2) unstable; urgency=low

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 16 May 2013 07:44:07 +0000

ceilometer (2013.1-1) experimental; urgency=low

  * Initial release (Closes: #693406).

 -- Thomas Goirand <zigo@debian.org>  Wed, 10 Apr 2013 13:19:50 +0800
